package oberfläche;

import java.awt.BorderLayout;
import javax.swing.JColorChooser;
import javax.swing.JComponent;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;
	private JLabel schreibText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 449, 742);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		schreibText = new JLabel("Dieser Text soll ver\u00E4ndert werden");
		schreibText.setBounds(33, 34, 315, 39);
		contentPane.add(schreibText);
		schreibText.setHorizontalAlignment(SwingConstants.CENTER);

		JTextPane txtpnHierStehtEin = new JTextPane();
		txtpnHierStehtEin.setText("Aufgabe 1. Hintergund \u00E4ndern");
		txtpnHierStehtEin.setBounds(12, 96, 340, 22);
		contentPane.add(txtpnHierStehtEin);

		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonRot_clicked();
			}
		});
		btnRot.setBounds(22, 131, 73, 25);
		contentPane.add(btnRot);

		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonGrn_clicked();
			}
		});
		btnGrn.setBounds(148, 131, 73, 25);
		contentPane.add(btnGrn);

		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlau_clicked();
			}
		});
		btnBlau.setBounds(22, 164, 73, 25);
		contentPane.add(btnBlau);

		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGelb_clicked();
			}
		});
		btnGelb.setBounds(265, 131, 73, 25);
		contentPane.add(btnGelb);

		JButton btnStandartFarbe = new JButton("Neutral");
		btnStandartFarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonStandart_clicked();
			}
		});
		btnStandartFarbe.setBounds(148, 164, 89, 25);
		contentPane.add(btnStandartFarbe);

		JButton btnAuswahl = new JButton("Auswahl");
		btnAuswahl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonAuswahl_clicked();
			}
		});
		btnAuswahl.setBounds(267, 164, 97, 25);
		contentPane.add(btnAuswahl);

		JTextPane aufgabe2 = new JTextPane();
		aufgabe2.setText("Aufgabe 2: Text formatieren");
		aufgabe2.setBounds(12, 212, 182, 22);
		contentPane.add(aufgabe2);

		JButton btnNewButton = new JButton("Arial");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonArial_clicked();
			}
		});
		btnNewButton.setBounds(12, 249, 97, 25);
		contentPane.add(btnNewButton);

		JButton btnCosmicSansMs = new JButton("Cosmic Sans MS");
		btnCosmicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonCosmicSansMs_clicked();
			}
		});
		btnCosmicSansMs.setBounds(125, 249, 144, 25);
		contentPane.add(btnCosmicSansMs);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			buttonCourierNew_clicked();}
		});
		btnCourierNew.setBounds(281, 249, 131, 25);
		contentPane.add(btnCourierNew);

		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("hier bitte Text eingeben");
		txtHierBitteText.setBounds(22, 293, 331, 22);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			buttonSchreiben_clicked();}
		});
		btnInsLabelSchreiben.setBounds(12, 328, 178, 25);
		contentPane.add(btnInsLabelSchreiben);
		
		JButton löschButton = new JButton("Text im Label l\u00F6schen");
		löschButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			buttonleer_clicked();}
		});
		löschButton.setBounds(202, 328, 182, 25);
		contentPane.add(löschButton);
		
		JTextPane txtpnAugabeSchriftfarbe = new JTextPane();
		txtpnAugabeSchriftfarbe.setText("Aufgabe 3: Schriftfarbe \u00E4ndern");
		txtpnAugabeSchriftfarbe.setBounds(12, 366, 239, 22);
		contentPane.add(txtpnAugabeSchriftfarbe);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			buttonschreibrot_clicked();}
		});
		btnRot_1.setBounds(12, 401, 97, 25);
		contentPane.add(btnRot_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			buttonschreibblau_clicked();}
		});
		btnBlau_1.setBounds(140, 401, 97, 25);
		contentPane.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			buttonschreibschwarz_clicked();}
		});
		btnSchwarz.setBounds(267, 401, 97, 25);
		contentPane.add(btnSchwarz);
		
		JTextPane txtpnSchriftgreVerndern = new JTextPane();
		txtpnSchriftgreVerndern.setText("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		txtpnSchriftgreVerndern.setBounds(12, 442, 330, 22);
		contentPane.add(txtpnSchriftgreVerndern);
		
		JButton button = new JButton("+");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			buttonPlus_clicked();}
		});
		button.setBounds(22, 477, 168, 25);
		contentPane.add(button);
		
		JButton button_1 = new JButton("-");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			buttonMinus_clicked();}
		});
		button_1.setBounds(225, 477, 168, 25);
		contentPane.add(button_1);
		
		JTextPane txtpnAufgabeTextausrichtung = new JTextPane();
		txtpnAufgabeTextausrichtung.setText("Aufgabe 5: Textausrichtung");
		txtpnAufgabeTextausrichtung.setBounds(12, 514, 336, 22);
		contentPane.add(txtpnAufgabeTextausrichtung);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			buttonLinks_clicked();}
		});
		btnLinksbndig.setBounds(12, 549, 115, 25);
		contentPane.add(btnLinksbndig);
		
		JButton btnMittig = new JButton("Zentriert");
		btnMittig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			buttonMittig_clicked();}
		});
		btnMittig.setBounds(148, 549, 115, 25);
		contentPane.add(btnMittig);
		
		JButton btnRechtsbndig = new JButton("Rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			buttonRechts_clicked();}
		});
		btnRechtsbndig.setBounds(281, 549, 113, 25);
		contentPane.add(btnRechtsbndig);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			buttonExit_clicked();}
		});
		btnExit.setBounds(69, 627, 283, 55);
		contentPane.add(btnExit);
		
		JTextPane txtpnAufgabeProgramm = new JTextPane();
		txtpnAufgabeProgramm.setText("Aufgabe 6: Programm beenden");
		txtpnAufgabeProgramm.setBounds(12, 594, 352, 22);
		contentPane.add(txtpnAufgabeProgramm);

	}

	protected void buttonExit_clicked() {
		System.exit(1);	
	}

	protected void buttonRechts_clicked() {
		schreibText.setHorizontalAlignment(SwingConstants.RIGHT);	
	}

	protected void buttonMittig_clicked() {
		schreibText.setHorizontalAlignment(SwingConstants.CENTER);
	}

	protected void buttonLinks_clicked() {
		schreibText.setHorizontalAlignment(SwingConstants.LEFT);
	}

	protected void buttonMinus_clicked() {
		int groesse = schreibText.getFont().getSize();
		schreibText.setFont(new Font("Arial", Font.PLAIN, groesse - 1));	
	}

	protected void buttonPlus_clicked() {
		int groesse = schreibText.getFont().getSize();
		schreibText.setFont(new Font("Arial", Font.PLAIN, groesse + 1));	
	}

	protected void buttonschreibschwarz_clicked() {
		schreibText.setForeground(Color.BLACK);
	}

	protected void buttonschreibblau_clicked() {
		schreibText.setForeground(Color.BLUE);	
	}

	protected void buttonschreibrot_clicked() {
		schreibText.setForeground(Color.RED);
	}

	protected void buttonleer_clicked() {
		schreibText.setText("");
	}

	protected void buttonSchreiben_clicked() {
		schreibText.setText(txtHierBitteText.getText());	
	}

	protected void buttonCourierNew_clicked() {
		schreibText.setFont(new Font("Courier New", Font.PLAIN, 12));
	}

	protected void buttonCosmicSansMs_clicked() {
		schreibText.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
	}

	protected void buttonArial_clicked() {
		schreibText.setFont(new Font("Arial", Font.PLAIN, 12));
	}

	protected void buttonAuswahl_clicked() {
		Color ausgewaehlteFarbe = JColorChooser.showDialog(null, "Farbauswahl", null);
		this.contentPane.setBackground(ausgewaehlteFarbe);
	}

	protected void buttonStandart_clicked() {
		this.contentPane.setBackground(Color.WHITE);
	}

	protected void buttonGelb_clicked() {
		this.contentPane.setBackground(Color.YELLOW);
	}

	protected void buttonBlau_clicked() {
		this.contentPane.setBackground(Color.BLUE);
	}

	protected void buttonGrn_clicked() {
		this.contentPane.setBackground(Color.GREEN);
	}

	public void buttonRot_clicked() {
		this.contentPane.setBackground(Color.RED);
	}
}
